# cpp_binding_python_cmake

A small example project of C ++ to python binding
You need to install
- boost
- python 3.8
- gcc (C++11 min)
- cmake (3.10 min)
- make

And after
- sudo ./make.sh
- python3.8 test.py

Sources:
- https://github.com/jwdinius/call-cpp-from-python-with-boost
- https://cpp.developpez.com/tutoriels/interfacer-cpp-python/
- https://stackoverflow.com/questions/46313063/build-hello-world-using-boost-python
- https://github.com/cfinch/Shocksolution_Examples
